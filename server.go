package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
)

func main() {
	directory := getenv("DIRECTORY", "/data")
	port := getenv("PORT", "3000")
	serve(directory, port)
}

func serve(directory string, port string) {
	fs := http.FileServer(http.Dir(directory))
	http.Handle("/", fs)

	log.Println(fmt.Sprintf("Serving files from %s on port %s.", directory, port))
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), nil))
}

func getenv(key string, value string) string {
	v := os.Getenv(key)
	if len(v) == 0 {
		v = value
	}
	return v
}
