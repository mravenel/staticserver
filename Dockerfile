FROM golang:alpine as builder

RUN adduser -D -g '' appuser

RUN mkdir -p /go/src/server/

ADD server.go /go/src/server/

RUN CGO_ENABLED=0 GOOS=linux go install -a -installsuffix cgo -ldflags '-extldflags "-static"' server

FROM scratch

COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /go/bin/server /app/server

EXPOSE 3000

VOLUME /data

USER appuser

CMD ["/app/server"]